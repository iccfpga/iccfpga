#!/bin/bash

function error {
	echo "error: $1"
	exit 1
}

REPO="$1"

rm $REPO -rf
git clone ssh://gitlab.com/iccfpga/$REPO || error "clone"
cd $REPO

branches="$( git branch -a | grep 'remotes/origin' | grep -v 'HEAD' | awk -F'/' '{ print $3 }' | xargs )"
rm $REPO -rf

for branch in $branches
do
	echo "branch: $branch"
	git clone ssh://gitlab.com/iccfpga/$REPO || error "clone"
	git checkout $branch
	rm .git/ -rf
	git init
	git checkout -b $branch
	git add .
	git commit -m "initial commit"
	git remote add origin ssh://git@gitlab.com/iccfpga/$REPO
	git push -u --force origin $branch
	cd -
	rm -rf $REPO
done

exit


